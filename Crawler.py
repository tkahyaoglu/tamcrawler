import sys
import re
import urllib2
import urlparse
import threading
import sqlite3 as sqlite
import robotparser
import MySQLdb
import time
import string
from string import whitespace as ws


# constants are defined
alpha = 0.1
inheritConstant = 0.02
neighborhoodConstant = 0.03
potentialConstant = 0.04

#topic vector and keywords are defined
T_Vec = {"wild": "8", "life": "8", "Cyprus": "5", "bird": "9", "butterfly": "8", "flora": "2", "fauna": "4"}
keywords = ["wild", "life", "Cyprus", "bird", "butterfly", "flora", "fauna"]



# Connect to the db and create the tables if they don't already exist
connection = MySQLdb.connect(host = "localhost",
                             user = "crawler",
                             passwd= "123",
                             db="crawler")
cursor = connection.cursor()


# Compile keyword, description and anchor link and text regular expressions 
descriptionregex = re.compile('<meta\sname=["\']description["\']\scontent=["\'](.*?)["\']\s/>')
keywordregex = re.compile('<meta\sname=["\']keywords["\']\scontent=["\'](.*?)["\']\s/>')
#linkregex = re.compile('<a.*\shref=[\'"](.*?)[\'"].*?>')
anchoregex = re.compile('<a.*\shref=[\'"](.*?)[\'"].*?>(.+)</a>', re.IGNORECASE)



#insert the starting urls
cursor.execute("INSERT INTO url_queue VALUES(%s, %s, %s, %s)", (0, 0, "http://en.wikipedia.org/wiki/Wildlife_of_Cyprus", 8))
cursor.execute("INSERT INTO url_queue VALUES(%s, %s, %s, %s)", (0, 0, "http://www.cyprusbutterflies.co.uk/index.html", 8))
cursor.execute("INSERT INTO url_queue VALUES(%s, %s, %s, %s)", (0, 0, "http://www.cypnet.co.uk/ncyprus/green/wildlife.htm", 8))
cursor.execute("INSERT INTO url_queue VALUES(%s, %s, %s, %s)", (0, 0, "http://www.cypenv.info/cypnat/files/butterflies.aspx", 8))
cursor.execute("INSERT INTO url_queue VALUES(%s, %s, %s, %s)", (0, 0, "http://en.wikipedia.org/wiki/Cyprus", 8))
cursor.execute("INSERT INTO url_queue VALUES(%s, %s, %s, %s)", (0, 0, "http://uk.reuters.com/article/2010/08/18/oukoe-uk-cyprus-reptile-idUKTRE67H0XJ20100818", 8))
cursor.execute("INSERT INTO url_queue VALUES(%s, %s, %s, %s)", (0, 0, "http://www.mlahanas.de/Cyprus/Fauna/SpeckledWood.html", 8))

connection.commit()



class threader ( threading.Thread ):

	# Parser for robots.txt that helps determine if we are allowed to fetch a url
	rp = robotparser.RobotFileParser()
	
	
	def run(self):
		while 1:
			try:
				# Get the first item from the queue
				cursor.execute("SELECT * FROM url_queue ORDER BY PRIORITY DESC LIMIT 1")
				crawling = cursor.fetchone()

				# Remove the item from the queue
				cursor.execute("DELETE FROM url_queue WHERE id = %s", (crawling[0]))
				connection.commit()
				
			except KeyError:
				raise StopIteration
			except:
				pass
			
			# if there is nothing in the queue, then set the status to done and exit
			if crawling == None:
				
				sys.exit("Done!")
			# Crawl the link
			self.crawl(crawling)
		
	
	def crawl(self, crawling):
		# crawler id
		cid = crawling[0]
		# parent id. 0 if start url
		pid = crawling[1]
		# current depth
		curl = crawling[2]
		# crawling urL
		priority = crawling[3]
		# Split the link into its sections
		url = urlparse.urlparse(curl)
		try:
			# Have our robot parser grab the robots.txt file and read it
			self.rp.set_url('http://' + url[1] + '/robots.txt')
			self.rp.read()
		
			# If we're not allowed to open a url, return the function to skip it
			if not self.rp.can_fetch('tamcrawler', curl):
				if verbose:
					print curl + " not allowed by robots.txt"
				return
		except:
			pass
			
		
		try:
			# Create a Request object
			request = urllib2.Request(curl)
			# Add user-agent header to the request
			request.add_header("User-Agent", "tamcrawler")
			# Build the url opener, open the link and read it into msg
			opener = urllib2.build_opener()
			msg = opener.open(request).read()
			
			
		except:
			# If it doesn't load, skip this url
			return
		# Find what's between the title tags
		startPos = msg.find('<title>')
		if startPos != -1:
			endPos = msg.find('</title>', startPos+7)
			if endPos != -1:
				title = msg[startPos+7:endPos]
		
		
		# Start keywords list with whats in the keywords meta tag if there is one
		keywordlist = keywordregex.findall(msg)
		if len(keywordlist) > 0:
			keywordlist = keywordlist[0]
		else:
			keywordlist = ""
			
		# find description if available	
		descriptionlist = descriptionregex.findall(msg)
		if len(descriptionlist) > 0:
			descriptionlist = descriptionlist[0]
		else:
			descriptionlist = ""
			
		
		
		# Get links and anchor text
		anchorText = anchoregex.findall(msg)
		
		# strip the tags
		text = self.strip_ml_tags(msg)
		#remove white spaces
		lastText = self.RemoveWhiteSpace(text)
		
		
	
		
		
		
		#compute revelance score
		Score = 0
		Score = self.revelanceScore(lastText)
		
		
		
		
		if Score > alpha:
			
			
			try:
				cursor.execute("INSERT INTO crawled_queue VALUES(%s, %s, %s, %s, %s, %s, %s)", (cid, pid, curl, title, lastText, keywordlist, descriptionlist))
				connection.commit()
			except:
				pass
			self.queue_links(url, anchorText, cid, Score)
			
			
		else:
			
			cursor.execute("INSERT INTO garbage_queue VALUES(%s, %s, %s)", (cid, pid, curl))
			connection.commit()
			self.garbage_links(url, anchorText, cid, pid)
				
		
	def RemoveWhiteSpace(self, text):
		
		newText = ' '.join(text.translate(None, string.whitespace[:5]).split())
		return newText
	
	def strip_ml_tags(self, in_text):
		
		# convert in_text to a list
		s_list = list(in_text)
		i,j = 0,0
		try:
			while i < len(s_list):
				# iterate until a left-angle bracket is found
				if s_list[i] == '<':
				
					while s_list[i] != '>':
						# pop everything from the the left-angle bracket until the right-angle bracket
						s_list.pop(i)

					# pops the right-angle bracket, too
					s_list.pop(i)
				else:
					i=i+1
		except IndexError:
			pass
		# convert the list back into text
		join_char=''
		return join_char.join(s_list)


	
	
	def revelanceScore(self, text):

		# count words of a text file
		
		words = 0
	
		fname = "MyText1.txt"
		fout = open(fname, "w")
		fout.write(text)
		fout.close()

		# read the file back in
		textf = open(fname, "r")

		# reads one line at a time
		for line in textf:
			if line.startswith('\n'):
				continue
			else:
				# use None to split at any whitespace regardless of length
				# so for instance double space counts as one space
				tempwords = line.split(None)
				words += len(tempwords)
		textf.close()

		uk = 0
		indx = 0
		for kkk in keywords:
			frequency = 0
	
			compiled = re.compile(kkk, re.IGNORECASE)
			text1 = compiled.findall(text)
			
			frequency = len(text1)
			try:
				uk = uk + (float(frequency) / float(words)) *  float(T_Vec[keywords[indx]])
			except ZeroDivisionError:
				uk = 0
				
			indx = indx + 1
		return uk
			
	def queue_links(self, url, links, cid, currentNodeScore):
			
			# Read the links  and insert them into the queue
			for link, a_text in links:
				inheritedScore = 0
				anchorScore = 0
				neighborhoodScore = 0
				potentialScore = 0
				cursor.execute("SELECT url FROM url_queue WHERE url=%s", [link])
				for row in cursor:
					if row[0].decode('utf8') == url:
						continue
				if link.startswith('/'):
					link = 'http://' + url[1] + link
				elif link.startswith('#'):
					continue
				elif not link.startswith('http'):
					link = urlparse.urljoin(url.geturl(),link)
				
				cursor.execute("SELECT * FROM garbage_queue where url=%s",[link])
				craw = cursor.fetchone()
				cursor.execute("SELECT * FROM crawled_queue where url=%s",[link])
				craw2 = cursor.fetchone()
				#if it is in crawled queue or garbage queue, then throw away the url. 
				#else calculate the revelance of child nodes
				if (craw != None) or (craw2 != None): 
					continue
				else:
					inheritedScore = inheritConstant * currentNodeScore
					anchorScore = self.revelanceScore(a_text)
					neighborhoodScore = neighborhoodConstant * anchorScore
					potentialScore = (potentialConstant * inheritedScore) + ((1 - potentialConstant) * neighborhoodScore)
					try:
						cursor.execute("INSERT INTO url_queue VALUES ( %s, %s, %s, %s )", (None, cid, link, potentialScore))
						connection.commit()
					except:
						pass
					
				
	def garbage_links(self, url, links, cid, pid):
			
			
			for link, a_text in links:
				
				cursor.execute("SELECT url FROM url_queue WHERE url=%s", [link])
				for row in cursor:
					if row[0].decode('utf8') == url:
						continue
				if link.startswith('/'):
					link = 'http://' + url[1] + link
				elif link.startswith('#'):
					continue
				elif not link.startswith('http'):
					link = urlparse.urljoin(url.geturl(),link)
				
				
				
				try:
					cursor.execute("INSERT INTO garbage_queue VALUES(%s, %s, %s)", (cid, pid, link))
					connection.commit()
				except:
					pass
					
				
if __name__ == '__main__':
	# Run main loop
	threader().run()

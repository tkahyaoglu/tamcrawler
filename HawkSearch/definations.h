/*
 *  definations.h
 *  Classifier
 *
 *  Created by Tansu Kahyaoglu on 21.05.2011.
 *  Copyright 2011 ODTU. All rights reserved.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mysql.h>
#include <ctype.h>
#include <time.h>
#include "stemmer.h"

#define LETTER(ch) (isupper(ch) || islower(ch))
#define INC 50           /* size units in which s is increased */

struct WORD
{
    int freq;
    char word[30];
	int loc[100];
};

typedef char * string;
int max_counter = 1404;
static int i_max = INC;  /* maximum offset in s */
static char * s;         /* a char * (=string) pointer; passed into b above */


void tokenize(char*);
int sameness (char *, struct WORD *,int);
int process (char *, struct WORD *,string *,int);
void compress_spaces(char *);
int containsWord(FILE *,char *);
void createTestVectorFile(string *, int );
void createTrainVectorFile(string *, int, int);
void copyFiles(FILE *, FILE *);
void stemString(string, string,FILE *);
//void getbigram(char **,int);

#include "sqlDef.h"

int main (int argc, char **argv)
{
	if (argc<2) {
		printf("usage: ./Classifier mode");
		exit(1);
	}
	mode = atoi(argv[1]);
	
	if (mode==0) {
		printf("Train Mode is active\n");
		dataLocation=2;
		classLocation=3;
		selectAllCQ = "select * from train_data";
	}
	else if(mode == 1){
		printf("Test Mode is active\n");
		dataLocation=4;
		classLocation=5;
		selectAllCQ = "select * from crawled_queue";
	}
	else {
		printf("Error: Please enter 0 for Train Mode, or 1 for Test Mode");
	}

	
	/*
	 *Define and Initialize Variables
	 */
	int n=0, k=0,class;
	MYSQL *conn;
	MYSQL_RES *res;	
	MYSQL_ROW row;
	
	struct WORD *w;
	w=(struct WORD *)malloc(sizeof(struct WORD)*1000000);

	string rawContent;
	string stemmedContent;
	string *wordList;
	
	FILE *foo,*vocab,*stopWordList,*trainFile,*output;
	
	
	/*
	 *Initialize Connection to MYSQL DB
	 */
	conn=mysql_init(NULL);
	if(!mysql_real_connect(conn, host, userName, password, dbName, 0, NULL, 0))
	{
		printf(" %s\n",mysql_error(conn));
	}
	
	/*
	 *Execute Query Select All 
	 */
	if(mysql_query(conn,selectAllCQ))
	{
		printf(" %s\n",mysql_error(conn));
		exit(1);
	}
	
	
	/*
	 *Open Necessary Files
	 */
	vocab = fopen("vocabulary.list", "a");
	stopWordList=fopen("stopword.list","r");
	if (stopWordList==NULL) {
		perror("Stopword.list");
		exit(1);
	}
	
	
	
	/*
	 *Store result of the query in ResultSet res
	 */
	res=mysql_store_result(conn);
	
	//For each row in DB
    while((row=mysql_fetch_row(res))!=NULL){

    	wordList = (char **)malloc(sizeof(char *)*strlen(row[dataLocation]));
        int c=0;
        for(c=0;c<strlen(row[dataLocation]);c++){
            wordList[c]=(char *)malloc(sizeof(char)*50);
        }
		
		//Initialize wordList and class for each Row
		int counter=0;
		rawContent = (char *)malloc(sizeof(char)*strlen(row[dataLocation]));
		strcpy(rawContent,row[dataLocation]);
		stemmedContent = (char *)malloc(sizeof(char)*strlen(row[dataLocation]));
		class = atoi(row[classLocation]);
		
		//Tokenization
		compress_spaces(rawContent);
		tokenize(rawContent);
        compress_spaces(rawContent);
		
		//Stemmer and Stop word Removal
		stemString(rawContent,stemmedContent,stopWordList);
		
		/*Create Vocabulary */
		counter=process(stemmedContent,w,wordList,counter);
		
		if (counter > max_counter) {
			max_counter = counter;
		}
		//fprintf(vocab, "%s\n",w[k].word);

     	for (k=0; k<counter; k++){
			if((((double)w[k].freq/counter) > 0.07))
			{
				
				if((strcmp(w[k].word,"0")!=0) && !(containsWord(stopWordList, w[k].word))){
					if (strcmp(w[k].word, "?")==0) {
						printf("gotcha");
					}
					fprintf(vocab, "%s\n",w[k].word);
				}
			}
		}
		
		createTrainVectorFile(wordList,counter,class);
		n++;
	}
	
	fclose(stopWordList);
	fclose(vocab);

	foo = fopen("foo", "r");
	if (foo==NULL) {
		perror("foo");
	}
	trainFile = fopen("train.dat", "w");
	fprintf(trainFile, "%i %i\n",n,max_counter+1);
	copyFiles(foo, trainFile);
	remove("foo");
	/*
	 *0 for train, 1 for test  
	 */
	if(mode==0)
	system("SVMTorch train.dat t.model");
	/*
	 *TEST MODE
	 */
	else if(mode==1){
		system("SVMTest -oa output t.model train.dat");
		
		float predict;
		output = fopen("output", "r");
		if (output == NULL) {
			perror("output");
		}
		k=0;
		while(fscanf(output, "%f",&predict)!=EOF)
		{
			if (predict>0) {
				mysql_data_seek(res, k);
				row = mysql_fetch_row(res);
				string SQL = "insert into crawled_queue_copy (crawlid,parentid,url,title,content,keywords) values (0,%d,'%s','%s','%s','%s')";
				string fullSQL = (string) malloc(sizeof(char)*(strlen(row[1])+strlen(row[2])+strlen(row[3])+strlen(row[4])+strlen(row[5])));
				strcpy(rawContent,row[4]);
				tokenize(rawContent);
				sprintf(fullSQL,SQL,atoi(row[1]),row[2],row[3],rawContent,row[5]);
				if(mysql_query(conn, fullSQL))
				{
					printf("\n %s\n",mysql_error(conn));
				}
				strcpy(rawContent,"");
			}
			k++;
		}
	}
	mysql_free_result(res);
	mysql_close(conn);
	return 0;
}

void tokenize(string wordList){
	char c;
	int i=0;
	while(i<=strlen(wordList)){
		c = wordList[i];
		if((c >= 33 && c<=47)||(c>=58 && c<=63) || (c>=91 && c<=95) || (c>=123 && c<127))
			wordList[i]=' ';
		else if(c>=65 && c<=90)
			wordList[i]=c+32;
		i++;
	}
}

/* Takes in a word, and compares it with existing words. Returns 0 if it has not previously been saved */
int sameness (char dummy[], struct WORD *w,int counter)
{
    int k=0;
    for (k=0; k<counter; k++)
        if (strcmp(dummy, w[k].word) == 0)
            return k;
    return -1;
}

int process (char *line, struct WORD *w,string *newWL,int counter)
{
    char *dummy;
    int x, y=1;
	int wordLocation=0;
	
    while(y!=0)
    {
	   if (strstr(line, " ") ==  NULL) //if word is at end of line
			y= 0;
        else
			*(strstr(line, " ")) = '\0'; //cut off point where first space is i.e. a word
		dummy = (string)malloc(sizeof(char)*strlen(line));
        strcpy(dummy, line); //copy to dummy variable
        strcpy(newWL[counter],dummy);
			//printf("\nwL: %s ",newWL[counter]);
      	wordLocation++;
		x = sameness (dummy, w, counter); //tests for sameness with other stored words
		
		if (x < 0) // i.e. new word
        {
         	strcpy(w[counter].word, dummy); //assign to new variable
			w[counter].freq = 1; //set initial frequency
			w[counter].loc[w[counter].freq-1]=counter;
			counter++;
		}
        else //word exists already
		{
			w[x].freq++; // increase frequency by one. x is the array
			// element no. of the existing word/
			w[x].loc[w[x].freq-1]=wordLocation;
		}
		
        if (*(strchr(line,'\0')+1) == ' ')
			line = strchr(line,'\0')+2; // punct might hv bn removed and replaced with space/
        else
			line = strchr(line, '\0')+1;
		/*\0 is removed; line now begins
		 there, so that loop may repeat,
		 and the next word be tested. We
		 add one because we do not want
		 to include the \0 in the 'new
		 line */
	}
	free(dummy);
	return counter;
}

void compress_spaces(char *str)
{
	char *dst = str;
	
	for (; *str; ++str) {
		*dst++ = *str;
		if (isspace(*str)) {
			do ++str; while (isspace(*str));
			--str;
		}
	}
	*dst = 0;
}

int containsWord(FILE * iFile,char *word){
	char  tmp[20]="";
	fseek(iFile, 0, SEEK_SET);
	while(fgets(tmp, sizeof(tmp),iFile)!=NULL)
	{
		if (strstr(tmp, word)){
			if(strlen(tmp)==(strlen(word)+1))
				return TRUE;
	}
	}
	return FALSE;
}

void createTestVectorFile(string *wordList, int counter){
	int i;
	FILE *vocab = fopen("vocabulary.list", "r");
	if (vocab==NULL) {
		perror("vocabulary.list");
	}
	FILE *trainFile = fopen("foo", "a");
	for (i=0; i<counter; i++) {
		fseek(vocab, 0, SEEK_SET);
		if (containsWord(vocab, wordList[i])) {
			fprintf(trainFile, "%i ",1);
		}
		else {
			fprintf(trainFile, "%i ",0);
		}
	}
	while (counter<max_counter) {
		fprintf(trainFile, "%i ",0);
		counter++;
	}
	fprintf(trainFile, "\n");
	fclose(vocab);
	fclose(trainFile);
}

void createTrainVectorFile(string *wordList, int counter, int class){
	int i;
	FILE *vocab = fopen("vocabulary.list", "r");
	if (vocab==NULL) {
		perror("vocabulary.list");
	}
	FILE *trainFile = fopen("foo", "a");
	for (i=0; i<counter; i++) {
		if (containsWord(vocab, wordList[i])) {
			fprintf(trainFile, "%i ",1);
		}
		else {
			fprintf(trainFile, "%i ",0);
		}
	}
	while (counter<max_counter) {
		fprintf(trainFile, "%i ",0);
		counter++;
	}
	fprintf(trainFile, "%i",class);
	fprintf(trainFile, "\n");
	fclose(vocab);
	fclose(trainFile);
}

void copyFiles(FILE *from,FILE *to){
	/* copy the file */
	char ch;

	while(!feof(from)) {
		ch = fgetc(from);
		if(ferror(from)) {
			printf("Error reading source file.\n");
			exit(1);
		}
		if(!feof(from)) fputc(ch, to);
		if(ferror(to)) {
			printf("Error writing destination file.\n");
			exit(1);
		}
	}
	
	if(fclose(from)==EOF) {
		printf("Error closing source file.\n");
		exit(1);
	}
	
	if(fclose(to)==EOF) {
		printf("Error closing destination file.\n");
		exit(1);
	}
	
}

void increase_s()
{  i_max += INC;
	{  char * new_s = (char *) malloc(i_max+1);
		{ int i; for (i = 0; i < i_max; i++) new_s[i] = s[i]; } /* copy across */
		free(s); s = new_s;
	}
}

void stemString(string rawContent,string stemmedContent,FILE *stopWordList)
{  
	int index=0;
	int j;
	int l=0;
	while(TRUE){
	{  
		s = (char *) malloc(sizeof(char)*(i_max+1));
		int ch = rawContent[l];
		if(ch == '\0') 
			return;
		if (LETTER(ch))			
		{  int i = 0;
			while(TRUE)
			{  
				if (i == i_max) 
					increase_s();
				
				s[i] = ch; 
				i++;
				l++;
				ch = rawContent[l];
				if (!LETTER(ch)) 
					break;
			}
			if(!containsWord(stopWordList,s)){
			s[stem(s,0,i-1)+1] = 0;
			/* the previous line calls the stemmer and uses its result to
			 zero-terminate the string in s */
			for (j=0; j<strlen(s); j++) {
				stemmedContent[index]=s[j];
				index++;
			}
			stemmedContent[index]=' ';
			index++;
			}
		}
		else{
			l++;
		}
		strcpy(s," ");
	}		
	}
	free(s);
}
